package com.demo.apipringboot.controllers;

import com.demo.apipringboot.dao.IArticle;
import com.demo.apipringboot.dao.IComment;
import com.demo.apipringboot.dao.IUtilisateur;
import com.demo.apipringboot.models.Article;
import com.demo.apipringboot.models.Comment;
import com.demo.apipringboot.models.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/Comment")
public class CommentController {
    @Autowired
    private IComment iComment;
    @Autowired
    private IArticle iArticle;
    @Autowired
    private IUtilisateur iUtilisateur;

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public class CommentNotFoundException extends RuntimeException {
        private String id;
        public CommentNotFoundException(String id) {
            super(String.format(" not found : '%s'", id));
            this.id = id;
        }
        public String getId() {
            return this.id;
        }
    }

    @GetMapping("/all")
    private List<Comment> getall() {
        return iComment.findAll();
    }

    @PostMapping("/save/{idArticle}/{idUtilisateur}")
    private Comment save(@RequestBody Comment comment, @PathVariable Long idArticle, @PathVariable Long idUtilisateur) {
        Article article = iArticle.getOne(idArticle);
        comment.setArticle(article);
        Utilisateur utilisateur = iUtilisateur.getOne(idUtilisateur);
        comment.setUtilisateur(utilisateur);
        comment.setCreatedAt(new Date());
        return iComment.save(comment);
    }

    @PutMapping("/update/{ID}")
    private Comment update(@RequestBody Comment comment, @PathVariable Long ID) {
        comment.setId(ID);
        Comment comment1 = iComment.getOne(ID);
        comment.setCreatedAt(comment1.getCreatedAt());
        comment.setUpdateAt(new Date());
        comment.setArticle(comment1.getArticle());
        comment.setUtilisateur(comment1.getUtilisateur());
        return iComment.saveAndFlush(comment);
    }

    @DeleteMapping("/delete/{id}")
    private HashMap<String, String> delete(@PathVariable Long id) {
        HashMap hashMap = new HashMap();
        try {
            iComment.deleteById(id);
            hashMap.put("etat", "Comment supprimer");
            return hashMap;
        } catch (Exception e) {
            hashMap.put("etat", "Comment  non supprimer");
            return hashMap;
        }
    }

    @GetMapping("/commentby/{id}")
    public Comment retrieveComment(@PathVariable long id) {
        Optional<Comment> comment = iComment.findById(id);

        if (!comment.isPresent())
            throw new CommentNotFoundException("id-" + id);

        return comment.get();
    }
    @GetMapping("/commentArt/{id}")
    public List<Comment> getAllCommentByarticle(@PathVariable Long id) {
//        List<Comment> commentList = new ArrayList<>();
//        for(Comment comment:iComment.findAll())
//            if(comment.getArticle().getId().equals(id))
//                commentList.add(comment);
        Article article=iArticle.getOne(id);
        return article.getComments();
    }

    }



