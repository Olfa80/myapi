package com.demo.apipringboot.controllers;

import com.demo.apipringboot.dao.IComment;
import com.demo.apipringboot.dao.IUtilisateur;
import com.demo.apipringboot.models.Comment;
import com.demo.apipringboot.models.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/utilisateur")
public class UtilisateurController {
    @Autowired
    private IUtilisateur iUtilisateur;
    @Autowired
    private IComment iComment;

    @GetMapping("/all")
    private List<Utilisateur> getall() {
        return iUtilisateur.findAll();
    }

    @PostMapping("/save")
    private Utilisateur save(@RequestBody Utilisateur utilisateur) {
        return iUtilisateur.save(utilisateur);
    }

    @PutMapping("/update/{ID}")
    private Utilisateur update(@RequestBody Utilisateur utilisateur, @PathVariable Long ID) {
        utilisateur.setId(ID);
        return iUtilisateur.saveAndFlush(utilisateur);
    }

    @DeleteMapping("/delete/{id}")
    private HashMap<String, String> delete(@PathVariable Long id) {
        HashMap hashMap = new HashMap();
        try {
            iUtilisateur.deleteById(id);
            hashMap.put("etat", "Utilisateur supprimer");
            return hashMap;
        } catch (Exception e) {
            hashMap.put("etat", "Utilisateur  non supprimer");
            return hashMap;
        }
    }
}
