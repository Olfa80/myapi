package com.demo.apipringboot.controllers;

import com.demo.apipringboot.dao.IArticle;
import com.demo.apipringboot.dao.IComment;
import com.demo.apipringboot.models.Article;
import com.demo.apipringboot.models.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/Article")
public class ArticleController {

    @Autowired
    private IArticle iArticle;
    @Autowired
    private IComment iComment;

    @GetMapping("/all")
    private List<Article> getall() {
        return iArticle.findAll();
    }

    @PostMapping("/save")
    private Article save(@RequestBody Article article) {
        article.setDatecreation(new Date());
        return iArticle.save(article);
    }

    @PutMapping("/update/{ID}")
    private Article update(@RequestBody Article article, @PathVariable Long ID) {
        article.setId(ID);
        return iArticle.saveAndFlush(article);
    }
    @DeleteMapping("/delete/{id}")
    private HashMap<String,String> delete(@PathVariable Long id){
        HashMap hashMap=new HashMap();
        try{
            iArticle.deleteById(id);
            hashMap.put("etat","Article supprimer");
            return hashMap;
        }
        catch (Exception e){
            hashMap.put("etat","Article  non supprimer");
            return hashMap;
        }
    }

}
