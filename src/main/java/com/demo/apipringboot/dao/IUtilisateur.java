package com.demo.apipringboot.dao;

import com.demo.apipringboot.models.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUtilisateur extends JpaRepository<Utilisateur,Long>{
}
