package com.demo.apipringboot.dao;

import com.demo.apipringboot.models.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IArticle  extends JpaRepository<Article,Long>{
}
