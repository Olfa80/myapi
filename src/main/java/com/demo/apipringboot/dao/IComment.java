package com.demo.apipringboot.dao;

import com.demo.apipringboot.models.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IComment extends JpaRepository <Comment,Long>{
}
