package com.demo.apipringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApipringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApipringbootApplication.class, args);
	}

}
