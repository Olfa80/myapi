package com.demo.apipringboot.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
public class Article implements Serializable {

   @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
    private String TitleArticle;
    private String designation;
    private String detailles;
    private Date datecreation;

    @OneToMany(mappedBy = "article")

    private List<Comment> comments;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getTitleArticle() {
        return TitleArticle;
    }

    public void setTitleArticle(String titleArticle) {
        TitleArticle = titleArticle;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDetailles() {
        return detailles;
    }

    public void setDetailles(String detailles) {
        this.detailles = detailles;
    }

    public Date getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(Date datecreation) {
        this.datecreation = datecreation;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public Article(String titleArticle, String designation, String detailles, Date datecreation, List<Comment> comments) {
        TitleArticle = titleArticle;
        this.designation = designation;
        this.detailles = detailles;
        this.datecreation = datecreation;
        this.comments = comments;
    }

    public Article() {
    }
}
