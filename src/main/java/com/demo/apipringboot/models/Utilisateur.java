package com.demo.apipringboot.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity

public class Utilisateur implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String Nom;

    @OneToMany(mappedBy = "utilisateur")
    @JsonBackReference
    public List<Comment> comment;

    public Long getId() {
        return id;
    }

    public String getNom() {
        return Nom;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public List<Comment> getComment() {
        return comment;
    }

    public void setComment(List<Comment> comment) {
        this.comment = comment;
    }

    public Utilisateur(String nom, List<Comment> comment) {
        Nom = nom;
        this.comment = comment;
    }

    public Utilisateur() {
    }
}
