package com.demo.apipringboot.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity

public class Comment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private  String Description;
    @ManyToOne
    private Utilisateur utilisateur;
    @ManyToOne
    @JsonBackReference
    private Article article;

    public Long getId() {
        return id;
    }


    private Date createdAt;
    private  Date updateAt;


    // date de Modification de commentaire
    public void setId(Long id) {
        this.id = id;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public Comment(Utilisateur utilisateur, Article article) {
        this.utilisateur = utilisateur;
        this.article = article;
    }

    public Comment(String description, Utilisateur utilisateur, Article article, Date createdAt, Date updateAt) {
        Description = description;
        this.utilisateur = utilisateur;
        this.article = article;
        this.createdAt = createdAt;
        this.updateAt = updateAt;
    }

    public Comment() {
    }
}
